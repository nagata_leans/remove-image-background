# Remove Image Background using Python
import sys
from rembg import remove
from PIL import Image

# コマンドライン引数から入力ファイル名を取得
if len(sys.argv) < 2:
    print("Usage: python script.py <input_file>")
    sys.exit(1)

input_path = sys.argv[1]
output_path = 'output.png'

# 画像を開く
input_image = Image.open(input_path)

# 背景を削除
output_image = remove(input_image)

# 結果を保存
output_image.save(output_path)
