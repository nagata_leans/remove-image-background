[Day 120 : Remove Image Background using Python - YouTube](https://youtu.be/RkdFkhfMK2k)

# REQUIREMENTS
- python 3.11.6

# 準備
```
pip install rembg
```

# 実行
```
python remove-image-background.py [入力ファイル]
```

出力は `output.png` です。
